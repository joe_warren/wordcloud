#!/usr/bin/env python
import sys
frequencies = {}

with open(sys.argv[1]) as f:
    for line in f:
         for word in line.split():
              if word in frequencies:
                    frequencies[word] += 1
              else:
                    frequencies[word] = 1

for word in frequencies:
    print("%s %d" %(word, frequencies[word]))
