#!/usr/bin/env python
from gensim import models
import sys

infile = sys.argv[1] 
outfile = sys.argv[2]

sentences = models.word2vec.LineSentence(sys.argv[1])
model = models.word2vec.Word2Vec(sentences, size=100, window=5, min_count=5, workers=4)
model.save(outfile)
