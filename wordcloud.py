#!/usr/bin/env python
import sys
import math
import string
import gensim
import numpy as np
from sklearn.manifold import TSNE
import itertools
from write_svg import write_svg

Word2Vec = gensim.models.Word2Vec
model = Word2Vec.load('vectors.bin')

stdFreqs = {}
with open("text.freqs") as f:
    stdFreqs = {line.split()[0]: float(line.split()[1]) for line in f}

def getTwoPrincipalComponents( A ):
    n = 2
    U, d , Vt = np.linalg.svd(A, full_matrices=False)
    return U[:, :n] * d[:n]

localFreqs = {}
with open(sys.argv[1]) as f:
    words = [word.lower() for line in f for word in line.split()]
    for word in words:
        if word in localFreqs:
             localFreqs[word] += 1.
        else:
             localFreqs[word] = 1.

wordScores = []
for word in localFreqs.keys():
     if word in stdFreqs and word in model:
         wordScores.append((word, localFreqs[word]/stdFreqs[word]))
wordScores.sort(cmp=lambda a,b: cmp(b[1],a[1]))
topWords = wordScores[0:75]
topWords, scores, models = zip(*map(lambda x: (x[0], x[1], (model[x[0]])), topWords))
modelArray = np.array(models)
tsne = TSNE()
#principalComponents = getTwoPrincipalComponents(modelArray)
principalComponents = tsne.fit_transform(modelArray)
components = map(lambda x: tuple(principalComponents[x[0]]), enumerate(topWords))
xcomponents, ycomponents = zip(*components)
xRange = (min(xcomponents), max(xcomponents))
yRange = (min(ycomponents), max(ycomponents))
maxScore = max(scores)
def normalizeValue(val, minMax):
    return (val-minMax[0])/(minMax[1]-minMax[0])
xfraction = map(lambda x: normalizeValue(x, xRange), xcomponents)
yfraction = map(lambda y: normalizeValue(y, yRange), ycomponents)
sizes = map(lambda score: score/maxScore, scores)

values = zip(topWords, sizes, xfraction, yfraction)

write_svg(values, sys.argv[2])
