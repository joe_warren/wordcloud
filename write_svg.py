from math import log
import cairo

SIZE = (600, 400)
MARGIN = 40
MAXWORDSIZE = 48
MINWORDSIZE = 6
WORDSIZERANGE = MAXWORDSIZE - MINWORDSIZE

def bounding_boxes_interfere(a, b):
    return not( b[0] >= a[2] or b[2] <= a[0] or b[3] <= a[1] or b[1] >= a[3])

def draw_bbox(ctx,bbox):
     ctx.move_to(0,0)
     ctx.set_source_rgba(0,0,1,0.5)
     ctx.rectangle(bbox[0], bbox[1], bbox[2]-bbox[0], bbox[3]-bbox[1]);
     ctx.fill()

def get_bbox(x, y, extents):
     (xstart,ystart, width, height,_,_) = extents
     return ( x+xstart -1,y+ystart-1,x+xstart+width+1, y+ystart+height+1)

def get_xy(bbox, extents):
    (xstart,ystart,_,_,_,_) = extents
    return (bbox[0]-xstart+1, bbox[1]-ystart+1)

def unintersecting_bbox(bbox, blocker, direction):
    """returns the x and y positions for a new bbox 
       that doesn't intersect the blocker"""
    width = bbox[2] - bbox[0] 
    height = bbox[3] - bbox[1]
    x = 0
    y = 0
    if direction == 0:
        x,y = (bbox[0], blocker[3])
    if direction == 1:
        x,y = (blocker[2], bbox[1])
    if direction == 2: 
        x,y = (bbox[0], blocker[1]-height)
    if direction == 3:
        x,y = (blocker[0]-width, bbox[1])
    return (x, y, x+width, y+height)

def explore_direction(direction, bbox, bboxes):
    while True:
        collisions = filter( lambda b: bounding_boxes_interfere(b, bbox),bboxes)
        if( len(collisions) == 0 ):
            return bbox
        bbox = unintersecting_bbox(bbox, collisions[0], direction)

def bbox_manhattan_distance(a, b):
   return abs(a[0]-b[0]) + abs(a[1]-b[1])

def get_unintersecting_bbox(bbox, bboxes):
    directions = range(0, 4)
    candidates = map(lambda d: explore_direction(d, bbox, bboxes), directions)
    distances = map(lambda c: bbox_manhattan_distance(c, bbox), candidates)
    index = distances.index(min(distances))
    return candidates[index]

def write_svg(values, filename):
    surf = cairo.SVGSurface(filename, SIZE[0], SIZE[1])
    ctx = cairo.Context(surf)

    bboxes = []
    for val in values:
        word = val[0]
        size = WORDSIZERANGE*log(1+val[1])/log(2)+ MINWORDSIZE
        x = MARGIN + (SIZE[0]-2*MARGIN) * val[2]
        y = MARGIN + (SIZE[1]-2*MARGIN) * val[3]

        ctx.set_font_size(size)
        extents = ctx.text_extents(word)
        width = extents[3]
        x -= width/2
        if x < 0: 
            x = 0
        if x+width > SIZE[0]:
            x = SIZE[0]-width
        bbox = get_unintersecting_bbox(get_bbox(x,y,extents), bboxes)
        x,y = get_xy(bbox, extents)

        ctx.move_to(x,y)
        bboxes.append(bbox)
        ctx.show_text(word)
    print(bboxes)
    surf.flush()
    surf.finish() 
